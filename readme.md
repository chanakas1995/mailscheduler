# CyberTech Mail Scheduler



## Installation

Rename .env.example file to .env and add MySql and Email credentials to it.(Email Configurations are mentioned below)

- ASSET_URL=/public
- MAIL_DRIVER=smtp
- MAIL_HOST=smtp.gmail.com
- MAIL_PORT=587
- MAIL_USERNAME=Your_Email_Address
- MAIL_PASSWORD=Your_Password (Generate app password in gmail)
- MAIL_ENCRYPTION=tls
- MAIL_FROM_ADDRESS=Your_Email_Address

Open the terminal inside project root directory and run 
```bash
composer update
``` 
```bash
php artisan key:generate
```
```bash
php artisan migrate:fresh
```
Restore the database backup file in to MySql server

## Usage

- Create a cron job as follow

```bash
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

- Rename the server.php in to index.php inside the root folder

- Move .htaccess file from public folder to root folder

- Access the project at localhost
