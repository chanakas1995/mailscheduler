-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.50-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema task_db
--

CREATE DATABASE IF NOT EXISTS task_db;
USE task_db;

--
-- Definition of table `attachements`
--

DROP TABLE IF EXISTS `attachements`;
CREATE TABLE `attachements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attachements`
--

/*!40000 ALTER TABLE `attachements` DISABLE KEYS */;
INSERT INTO `attachements` (`id`,`path`,`email_id`,`created_at`,`updated_at`) VALUES 
 (1,'Files/U8OfJ2BlJ71599641510.webp',1,'2020-09-09 08:51:50','2020-09-09 08:51:50'),
 (2,'Files/rXUa2zIBmm1599641510.jpg',1,'2020-09-09 08:51:50','2020-09-09 08:51:50');
/*!40000 ALTER TABLE `attachements` ENABLE KEYS */;


--
-- Definition of table `batches`
--

DROP TABLE IF EXISTS `batches`;
CREATE TABLE `batches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `batch_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `batches`
--

/*!40000 ALTER TABLE `batches` DISABLE KEYS */;
INSERT INTO `batches` (`id`,`batch_number`,`user_id`,`created_at`,`updated_at`) VALUES 
 (1,'Batch 01',1,'2020-09-09 08:49:29','2020-09-09 08:49:29');
/*!40000 ALTER TABLE `batches` ENABLE KEYS */;


--
-- Definition of table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contact_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` (`id`,`contact_id`,`name`,`number`,`email`,`batch_id`,`created_at`,`updated_at`) VALUES 
 (1,'101','Chanaka Sandaruwan','717367930','csgamage95@gmail.com',1,'2020-09-09 08:49:29','2020-09-09 08:49:29'),
 (2,'102','Nimal Perera','747858965','nimalp65496@gmail.com',1,'2020-09-09 08:49:29','2020-09-09 08:49:29');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;


--
-- Definition of table `email_has_batches`
--

DROP TABLE IF EXISTS `email_has_batches`;
CREATE TABLE `email_has_batches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` bigint(20) NOT NULL,
  `email_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_has_batches`
--

/*!40000 ALTER TABLE `email_has_batches` DISABLE KEYS */;
INSERT INTO `email_has_batches` (`id`,`batch_id`,`email_id`,`created_at`,`updated_at`) VALUES 
 (1,1,1,'2020-09-09 08:51:49','2020-09-09 08:51:49');
/*!40000 ALTER TABLE `email_has_batches` ENABLE KEYS */;


--
-- Definition of table `email_messages`
--

DROP TABLE IF EXISTS `email_messages`;
CREATE TABLE `email_messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `send_date` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_messages`
--

/*!40000 ALTER TABLE `email_messages` DISABLE KEYS */;
INSERT INTO `email_messages` (`id`,`alias`,`subject`,`body`,`send_date`,`status`,`created_at`,`updated_at`) VALUES 
 (1,'Marketing Email','Hooray! This is the email subject','<p>This is a <b>Sample Email <span style=\"background-color: rgb(255, 255, 0);\"><u>Body</u></span></b></p><p><b><span style=\"background-color: rgb(255, 255, 0);\"><u><br></u></span></b></p>','09/09/2020 2:19 PM',0,'2020-09-09 08:51:47','2020-09-09 08:51:47');
/*!40000 ALTER TABLE `email_messages` ENABLE KEYS */;


--
-- Definition of table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`,`migration`,`batch`) VALUES 
 (1,'2014_10_12_000000_create_users_table',1),
 (2,'2014_10_12_100000_create_password_resets_table',1),
 (3,'2020_09_08_020136_create_contacts_table',1),
 (4,'2020_09_08_020240_create_batches_table',1),
 (5,'2020_09_08_020258_create_email_messages_table',1),
 (6,'2020_09_08_020412_create_email_has_batches_table',1),
 (7,'2020_09_08_020505_create_attachements_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


--
-- Definition of table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;


--
-- Definition of table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) VALUES 
 (1,'Chanaka Sandaruwan','csgamage95@gmail.com','$2y$10$ySd.G51u0Cg6vLJMThjsy.VX5kIE/HzjHz1/EsUjbrznT0AlBJbYW','Bwad4bZHoEc0DfENe1q4wFnmTjkrtl0d318QHtCZkTGMz9c1Js68qc6kWY3s','2020-09-09 08:49:01','2020-09-09 08:49:01'),
 (2,'Chandika Sithuruwan','chandika95462@gmail.com','$2y$10$Uqr1GgQV4y0w80cEAsYSsuN6PlGN6.UxpNN65INmwFKjOs6fdCs6C','Dzjt50IPYUn00qqTB99JEkoOy6CzgVG31PVgmF1EjGKGqOT8NrGgHueC0kSd','2020-09-09 08:52:43','2020-09-09 08:52:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
