<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('emailscheduler')->group(function() {
    Route::get('/', 'EmailSchedulerController@index');
    Route::get('/create', 'EmailSchedulerController@create');
    Route::post('/schedule', 'EmailSchedulerController@store');
});
