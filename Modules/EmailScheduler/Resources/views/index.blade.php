@extends('emailscheduler::layouts.master')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-clock mr-2" aria-hidden="true"></i>Schedule Email</h3>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <form enctype="multipart/form-data" id="form">
            {{ csrf_field() }}
            <div class="form-group" style="position: relative">
                <label for="exampleInputEmail1">Send Date</label>
                <input type="text" class="form-control datetimepicker" name="send_date" id="send_date">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Batches</label>
                <select class="select2 form-control" name="batches[]" id="selectBox" multiple="multiple"
                    placeholder="Select Batches">
                    @foreach ($batches as $batch)
                        <option value="{{ $batch->id }}">{{ $batch->batch_number }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email Alias</label>
                <input type="text" class="form-control" id="alias" name="alias" aria-describedby="emailHelp"
                    placeholder="Email Alias">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email Subject</label>
                <input type="text" class="form-control" id="subject" name="subject" aria-describedby="emailHelp"
                    placeholder="Email Subject">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email Body</label>
                <input id="body" name="body" type="hidden">
                <div id="editor"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Attachements</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="attachements" name="attachements[]" multiple>
                </div>
            </div>            
            <div class="alert alert-success d-none" role="alert" id="message">
                This is a primary alert—check it out!
            </div>
        </form>
        <button id="saveButton" class="btn btn-primary"><i class="fas fa-save mr-2"></i>Submit</button>
        <button id="cancelButton" class="btn btn-danger"><i class="fas fa-times mr-2" type="button"></i>Cancel</button>
    </div>
    <!-- /.box -->
@endsection


@section('scripts')
    <script>
        // ClassicEditor
        //     .create(document.querySelector('#editor'))
        //     .then(editor => {
        //         console.log(editor);
        //     })
        //     .catch(error => {
        //         console.error(error);
        //     });

    </script>
    <script>
        $('document').ready(function() {
            function clearForm(){
                $("#form").trigger('reset');
                $('#message').removeClass("d-block");
                $('#message').addClass("d-none");
                $('#message').html("");
                $("#selectBox").val([]).change();
                $('.datetimepicker').data("DateTimePicker").date(moment(new Date).format('DD/MM/YYYY HH:mm'));                
                $('#editor').summernote("reset");
            }

            $('#cancelButton').on('click', function(event) {
                clearForm();
            });
            var dateNow = new Date();
            $('.datetimepicker').datetimepicker({
                defaultDate: dateNow
            });
            $('#selectBox').select2({
                placeholder: "Select Batches",
                allowClear: true,
                theme: "classic"
            });
            $('#saveButton').on('click', function(event) {
                $('#form').submit();
            });
            $('#form').on('submit', function(event) {
                var html = $('#editor').summernote('code');
                $('#body').val(html);
                $('#message').removeClass("d-block");
                $('#message').addClass("d-none");
                $('#message').html("");
                event.preventDefault();
                $.ajax({
                    url: 'emailscheduler/schedule',
                    type: 'post',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        if (data.errors != undefined) {
                            $('#message').removeClass("alert-success");
                            $('#message').removeClass("d-none");
                            $('#message').addClass("alert-danger");
                            $.each(data.errors, function(key, value) {
                                $('#message').append(value + "<br>");
                            });
                        }

                        if (data.success != undefined) {
                            clearForm();
                            $('#message').removeClass("alert-danger");
                            $('#message').removeClass("d-none");
                            $('#message').addClass("alert-success");
                            $('#editor').summernote("reset");
                            var dateNow = new Date();
                            $.each(data.success, function(key, value) {
                                $('#message').append(value + "<br>");
                            });
                        }
                    }
                });
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#editor').summernote();
        });
    </script>
@endsection
