<?php

namespace Modules\EmailScheduler\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller;
use App\Custom\ValidationRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Batch;
use App\Contact;
use App\Attachement;
use App\EmailHasBatch;
use App\EmailMessage;
use Excel;
use App\Custom\FileUpload;
use Illuminate\Support\Facades\Auth;

class EmailSchedulerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('emailscheduler::index')->with('batches', Auth::user()->batches);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('emailscheduler::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Request::all(), ValidationRule::scheduleValidation());
        if ($validator->fails()) {
            $error = new MessageBag(['errors' => $validator->errors()]);
            return Response::json($error);
        } else {
            $data = Request::except('batches');
            $email = EmailMessage::create($data);
            foreach (Request::get('batches') as $batch) {
                EmailHasBatch::create(["email_id"=>$email->id,"batch_id"=>$batch]);
            }
            $result=FileUpload::upload('attachements'); 
            foreach ($result as $attachement) {
                Attachement::create(["path"=>$attachement,"email_id"=>$email->id]);
            }          
            $error = new MessageBag(['success' => "Email Successfully Scheduled"]);
            return Response::json($error);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('emailscheduler::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('emailscheduler::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
