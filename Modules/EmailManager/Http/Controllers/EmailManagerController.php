<?php

namespace Modules\EmailManager\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller;
use App\Custom\ValidationRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use App\Batch;
use App\Contact;
use App\ImportContact;
use Excel;
use Illuminate\Support\Facades\Auth;

class EmailManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('emailmanager::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('emailmanager::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Request::all(), ValidationRule::contactListValidation());
        if ($validator->fails()) {
            $error = new MessageBag(['errors' => $validator->errors()]);
            return Response::json($error);
        } else {
            if (Batch::where('batch_number', Request::get('batch_number'))->exists()) {                
                $error = new MessageBag(['errors' => "Batch Number Already Exists"]);
                return Response::json($error);
            }else{
                $data = Request::except('contacts_list');
                $data["user_id"]=Auth::user()->id;
                $batch = Batch::create($data);
                $_SESSION['batch_id']=$batch->id;
                if (Request::hasFile('contacts_list')) {
                    $path = Request::file('contacts_list')->getRealPath();
                    Excel::import(new ImportContact, $path);
                } else {
                    $error = new MessageBag(['errors' => "File Not Found"]);
                    return Response::json($error);
                }
                unset($_SESSION['batch_id']);
                $error = new MessageBag(['success' => "Contacts List Successfully Uploaded"]);
                return Response::json($error);
            }
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('emailmanager::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('emailmanager::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function sample()
    {
        return redirect(asset('/ContactList.xlsx'));
    }
}
