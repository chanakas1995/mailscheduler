@extends('emailmanager::layouts.master')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-upload mr-2" aria-hidden="true"></i>Upload Contacts List</h3>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <form enctype="multipart/form-data" id="form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="exampleInputEmail1">Batch Number</label>
                <input type="text" class="form-control" id="batch_number" name="batch_number" aria-describedby="emailHelp"
                    placeholder="Batch Number">
                <small id="emailHelp" class="form-text text-muted">Batch Number Should Be Uniquley Identified</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Contact List File</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="contacts_list" name="contacts_list">
                </div>
                <small id="emailHelp" class="form-text text-muted">File Should Be According To The Sample Format</small>
            </div>
            <div class="alert alert-success d-none" role="alert" id="message">
                This is a primary alert—check it out!
            </div>
        </form>
        <button id="saveButton" class="btn btn-primary"><i class="fas fa-save mr-2" type="submit"></i>Submit</button>
        <button id="cancelButton" class="btn btn-danger"><i class="fas fa-times mr-2" type="reset"></i>Cancel</button>
    </div>
    </div>
    <!-- /.box -->
@endsection


@section('scripts')
    <script>
        $('document').ready(function() {
            function clearForm(){                
                $("#form").trigger('reset');
                $('#message').removeClass("d-block");
                $('#message').addClass("d-none");
                $('#message').html("");
            }
            $('#cancelButton').on('click', function(event) {
                clearForm();
            });
            $('#saveButton').on('click', function(event) {
                $("#form").submit();
            });
            $('#form').on('submit', function(event) {
                $('#message').removeClass("d-block");
                $('#message').addClass("d-none");
                $('#message').html("");
                event.preventDefault();
                $.ajax({
                    url: 'emailmanager/upload',
                    type: 'post',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        if (data.errors != undefined) {
                            $('#message').removeClass("alert-success");
                            $('#message').removeClass("d-none");
                            $('#message').addClass("alert-danger");
                            $.each(data.errors, function(key, value) {
                                $('#message').append(value + "<br>");
                            });
                        }

                        if (data.success != undefined) {                            
                            clearForm();
                            $('#message').removeClass("alert-danger");
                            $('#message').removeClass("d-none");
                            $('#message').addClass("alert-success");
                            $.each(data.success, function(key, value) {
                                $('#message').append(value + "<br>");
                            });
                        }
                    }
                });
            });
        });

    </script>
@endsection
