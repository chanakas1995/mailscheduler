<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use App\EmailMessage;
use Carbon\Carbon;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emailMessages = EmailMessage::where('send_date', '<=', Carbon::now()->format('m/d/Y h:i A'))->where('status', '0')->get();
        foreach ($emailMessages as $emailMessage) {
            $mail = new PHPMailer(true);
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->Host       = env('MAIL_HOST', '');
            $mail->SMTPAuth   = true;
            $mail->Username   = env('MAIL_USERNAME', '');
            $mail->Password   = env('MAIL_PASSWORD', '');
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port       = env('MAIL_PORT', '');
            $mail->setFrom(env('MAIL_USERNAME', ''), $emailMessage->alias);
            $mail->addReplyTo(env('MAIL_USERNAME', ''), '');
            $mail->isHTML(true);
            $mail->Subject = $emailMessage->subject;
            $mail->Body    = $emailMessage->body;
            foreach ($emailMessage->batches as $batch) {
                foreach ($batch->contacts as $contact) {
                    $mail->addAddress($contact->email, $contact->name);
                }
            }
            try {
            if ($emailMessage->attachements->count()>0) {
                foreach ($emailMessage->attachements as $attachement) {
                    $mail->addAttachment(public_path()."/".$attachement->path);
                }
            }
                $mail->send();
            } catch (Exception $e) {
                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            }
            $emailMessage->status=1;
            $emailMessage->save();
        }

    }
}
