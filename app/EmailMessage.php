<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailMessage extends Model
{
    protected $fillable = [
        'alias','send_date','subject','body'
    ];    

    public function batches()
    {
        return $this->belongsToMany('App\Batch','email_has_batches','email_id','batch_id');
    }

    public function attachements()
    {
        return $this->hasMany('App\Attachement','email_id');
    }
}
