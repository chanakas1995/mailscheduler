<?php
namespace App\Custom;

use App\Rules\MaxWordsRule;

class ValidationRule {
    public static function contactListValidation() {
        return array(
            'batch_number' => 'required',
            'contacts_list' => 'required',
        );
    }

    public static function scheduleValidation() {
        return array(
            'send_date' => 'required',
            'batches' => 'required',
            'alias' => 'required',
            'subject' => 'required',
            'body' => array('required',new MaxWordsRule()),
        );
    }
    
}
