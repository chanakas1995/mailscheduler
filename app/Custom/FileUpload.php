<?php
namespace App\Custom;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;


class FileUpload
{
    public static function upload($fileInput)
    {
        $result = array();
        foreach (Request::file($fileInput) as $file) {
            $newName = Str::random(10) . (now()->timestamp) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('Files/'), $newName);
            array_push($result,'Files/'. $newName);
        }
        return $result;
    }
}
