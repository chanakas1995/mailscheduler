<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailHasBatch extends Model
{
    protected $fillable = [
        'batch_id','email_id'
    ];
}
