<?php
namespace App;

use App\Contact;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportContact implements ToModel
{
    public function model(array $row)
    {
        if ($row[0]=="Id" || $row[1]=="Name" || $row[2]=="Number" || $row[3]=="E-mail") {
            return null;
        }else{
            return new Contact([
            'contact_id' => $row[0],
            'name' => $row[1],
            'number' => $row[2],
            'email' => $row[3],
            'batch_id' => $_SESSION['batch_id'],
            ]);
        }
    }
}
