<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = [
        'batch_number','user_id'
    ];

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }
}
