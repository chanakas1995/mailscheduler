<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachement extends Model
{
    protected $fillable = [
        'path','email_id'
    ];
}
